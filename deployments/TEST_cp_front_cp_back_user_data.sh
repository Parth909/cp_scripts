#!/bin/bash

TIMEFORMAT='===> Elaspsed time %R seconds'

time {

# Installing Node.js with Apt using NodeSource PPA

sudo apt update

# sudo apt upgrade -y :- Causes the script to exit after this command

sudo apt install curl -y

cd ~

echo "===> Installing Node 16.x"
curl -sL https://deb.nodesource.com/setup_16.x -o /tmp/nodesource_setup.sh

sudo bash /tmp/nodesource_setup.sh

sudo apt install nodejs

sudo npm install --location=global yarn@1.22.10

sudo npm install --location=global pm2

# if HOME is not set, otherwise pm2 will use /etc/.pm2 for logs & other things
export HOME=/home/ubuntu

# start pm2 on startup
pm2 startup

#install git
sudo apt install git -y

cd /home/ubuntu

echo "===> Cloning creds"

git clone https://cp_sec_dt_web_server:8ReZxYPt8RH3-B784w5H@gitlab.com/coin_planet/secrets.git

echo "===> Cloning cp-front"
# get source code from gitlab
git clone https://cp_front_dt_web_server:QgipydA9wyVfkoRuW2Am@gitlab.com/coin_planet/cp-front.git

cp ~/secrets/cp_front.env.local cp-front/

#get in project dir
cd cp-front

# renaming
sudo mv cp_front.env.local .env.local

#give permission
sudo chmod -R 755 .

# In "next.config.js" find "http://localhost:8081/api" and replace it with "http://ip_addr:8081/api" in test environment, or "https://coinplanet.in:8081/api" in prod environment
export EC2_PUBLIC_IPV4=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)

sudo sed -i "s/http:\/\/localhost:8081\/api/http:\/\/$EC2_PUBLIC_IPV4:8081\/api/g" next.config.js

#install node module
yarn install

yarn build

echo "===> Starting cp-front"
# starting with pm2
pm2 start yarn --name "cpfront" -- start_prod

cd ..

echo "===> Cloning cp-back"
# get source code from gitlab
git clone https://cp_back_dt_web_server:qbPDxju2YNJPJvuKjjQc@gitlab.com/coin_planet/cp-back.git

# cp supports copy & rename only in the same dir
cp ~/secrets/cp_back.env.prod cp-back/

#get in project dir
cd cp-back

# renaming
mv cp_back.env.prod .env.prod

# In .env.prod change the "CLIENT_URL" in order to be allowed by CORS policy
sudo sed -i "s/CLIENT_URL=http:\/\/localhost:3000/CLIENT_URL=http:\/\/$EC2_PUBLIC_IPV4:3000/g" .env.prod

# A variable will be used for PRIVATE_DB_IP_ADDRESS
sudo sed -i "s/DATABASE_URL=postgres:\/\/username:password@ip_addr:port_no\/db_name/DATABASE_URL=postgres:\/\/postgres:chananHanan5775\$\$doolff@10.0.1.12:5432\/cpproddb/g" .env.prod

#give permission
sudo chmod -R 755 .
#install node modules
yarn install

echo "===> Starting cp-back"
# starting with pm2
pm2 start yarn --name "cpback" -- start_prod

cd ..

echo "===> Removing creds"

rm -r secrets

# giving user "ubuntu" permission to access the pm2 processes
sudo chown ubuntu:ubuntu /home/ubuntu/.pm2/rpc.sock /home/ubuntu/.pm2/pub.sock 

pm2 logs 0 &

pm2 logs 1 &

}