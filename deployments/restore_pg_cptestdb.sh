#!/bin/bash

check_exec(){
  if [ "$?" -ne 0 ]
  then 
    echo $1
    exit 1
  fi
}

sudo -u postgres -i

check_exec "===> Error : Cannot switch to user postgres"

psql -c "DROP DATABASE cptestdb;"

check_exec "===> Error : Cannot drop database cptestdb"

psql -c "CREATE DATABASE cptestdb;"

check_exec "===> Error : Cannot create database cptestdb"

psql -d cptestdb < /home/ubuntu/cp-front/cptestdb.sql

echo "===> Script ran successfully"

exit 0